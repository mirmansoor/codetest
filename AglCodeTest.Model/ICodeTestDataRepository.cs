﻿using System.Collections.Generic;

namespace AglCodeTest.Model
{
    public interface ICodeTestDataRepository
    {
        IEnumerable<Pet> GetPets();
    }
}