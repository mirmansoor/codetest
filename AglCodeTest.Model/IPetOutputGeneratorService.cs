﻿using System.Collections.Generic;

namespace AglCodeTest.Model
{
    public interface IPetOutputGeneratorService
    {
        IEnumerable<string> Generate();
    }
}