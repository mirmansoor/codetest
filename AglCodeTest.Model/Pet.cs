﻿namespace AglCodeTest.Model
{
    public class Pet
    {
        public string Name { get; set; }
        public string OwnersGender { get; set; }
    }
}