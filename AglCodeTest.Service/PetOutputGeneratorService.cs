﻿using System.Collections.Generic;
using System.Linq;
using AglCodeTest.Model;

namespace AglCodeTest.Service
{
    public class PetOutputGeneratorService : IPetOutputGeneratorService
    {
        private readonly ICodeTestDataRepository _repository;

        public PetOutputGeneratorService(ICodeTestDataRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<string> Generate()
        {
            var allPets = _repository.GetPets();

            var sortedPets = allPets.OrderBy(p => p.Name);
            foreach (var sortedPetGroup in sortedPets.GroupBy(s => s.OwnersGender))
            {
                yield return ($"***{sortedPetGroup.Key}***");
                foreach (var pet in sortedPetGroup)
                {
                    yield return pet.Name;
                }
            }
        }
    }
}