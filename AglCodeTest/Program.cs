﻿using System;
using System.Linq;
using AglCodeTest.Repository;
using AglCodeTest.Service;

namespace AglCodeTest.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                foreach (var item in new PetOutputGeneratorService(
                        new CodeTestDataRepository())
                    .Generate())
                {
                    System.Console.WriteLine(item);
                }
            }
            catch (CodeTestDataException ex)
            {
                System.Console.WriteLine(ex.Message);
            }

            System.Console.ReadLine();
        }
    }
}
