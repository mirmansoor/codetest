using System.Collections.Generic;
using System.Linq;
using AglCodeTest.Model;
using AglCodeTest.Repository.DataModel;

namespace AglCodeTest.Repository
{
    public static class PetModelTransformer
    {
        public static IEnumerable<Pet> Create(Person person)
        {
            return person.Pets?
                       .Select(p => new Pet
                       {
                           Name = p.Name,
                           OwnersGender = person.Gender
                       })
                   ?? Enumerable.Empty<Pet>();
        }
    }
}