﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AglCodeTest.Repository.Proxy
{
    public class AglWebClientProxy : IDisposable
    {
        private readonly HttpClient _httpClient;

        public AglWebClientProxy()
        {
            if (_httpClient == null)
                _httpClient = new HttpClient();
        }

        public async Task<T> Get<T>(Uri uri)
        {
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await _httpClient.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}