﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using AglCodeTest.Model;
using AglCodeTest.Repository.DataModel;
using AglCodeTest.Repository.Proxy;

namespace AglCodeTest.Repository
{
    public class CodeTestDataRepository : ICodeTestDataRepository
    {
        public IEnumerable<Pet> GetPets()
        {
            using (var aglClient = new AglWebClientProxy())
            {
                try
                {
                    var persons = aglClient.Get<IEnumerable<Person>>(
                        new Uri("http://agl-developer-test.azurewebsites.net/people.json")).Result;

                    if (persons == null)
                        throw new CodeTestDataException("Unable to parse data from code test webservice");

                    return persons.SelectMany(PetModelTransformer.Create);
                }
                catch (AggregateException ex)
                {
                    throw new CodeTestDataException($"Unable to retrieve data. {(ex.InnerException as HttpRequestException)?.Message}");
                }
            }
        }
    }
}
