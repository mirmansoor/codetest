using System;

namespace AglCodeTest.Repository
{
    public class CodeTestDataException : Exception
    {
        public CodeTestDataException(string message) :base(message)
        {
        }
    }
}