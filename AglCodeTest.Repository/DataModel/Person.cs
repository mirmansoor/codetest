﻿using System.Collections.Generic;

namespace AglCodeTest.Repository.DataModel
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public List<PetDataModel> Pets { get; set; }
    }
}