﻿namespace AglCodeTest.Repository.DataModel
{
    public class PetDataModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}