### What is this repository for? ###

* Code test solution for agl. Used DDD layers and proxy pattern.
* AglCodeTest.Console - Created console app that outputs the result from the Service layer.
* AglCodeTest.Model - Holds the main model for ouput and contracts for service and repository.
* AglCodeTest.Repository - Used Proxy pattern to implement agl web service consumption and exposed model through repository implemenation.
* AglCodeTest.Service - The service layer implemenation to expose data to consumers.
* Used dotnet core 2.
* Iterator pattern for generating output
* XUnit and MOQ for unit testing
* test with tofee