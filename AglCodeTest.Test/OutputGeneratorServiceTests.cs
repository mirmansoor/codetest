using System.Collections.Generic;
using System.Linq;
using AglCodeTest.Model;
using AglCodeTest.Repository.DataModel;
using AglCodeTest.Service;
using Moq;
using Xunit;

namespace AglCodeTest.Test
{
    public class OutputGeneratorServiceTests
    {
        [Theory, MemberData(nameof(PetsData))]
        public void GivenAListOfPetsAndOwnersGenders_WhenOutputGeneratorIsCalled_ReturnsOrderedListOfPetsGroupedUnderOwnerOwnersGender(
            IEnumerable<Pet> pets, 
            IEnumerable<string> outputLines)
        {
            var moqrepo = new Mock<ICodeTestDataRepository>();
            moqrepo.Setup(r => r.GetPets()).Returns(pets);

            var result = new PetOutputGeneratorService(moqrepo.Object).Generate();
            Assert.Equal(outputLines, result);
        }

        public static IEnumerable<object[]> PetsData => new[]
        {
            new object[]
            {
                new[]
                {
                    new Pet(){ Name = "Gizmo", OwnersGender = "Female"},
                    new Pet(){Name = "Alpha", OwnersGender = "Female"},
                    new Pet(){Name = "Jasper", OwnersGender = "Female"},
                    new Pet(){Name = "Molly", OwnersGender = "Male"},
                    new Pet(){Name = "Angel", OwnersGender = "Male"},
                    new Pet(){Name = "Tigger", OwnersGender = "Male"},
                },
                new[] {"***Female***", "Alpha", "Gizmo", "Jasper", "***Male***", "Angel", "Molly", "Tigger"  }
            },
            new object[]
            {
                new[]
                {
                    new Pet(){ Name = "Gizmo", OwnersGender = "Female"},
                    new Pet(){Name = "Jasper", OwnersGender = "Female"},
                    new Pet(){Name = "Molly", OwnersGender = "Male"},
                    new Pet(){Name = "Angel", OwnersGender = "Male"},
                    new Pet(){Name = "Tigger", OwnersGender = "Male"},
                },
                new[] {"***Male***", "Angel", "Molly", "Tigger", "***Female***", "Gizmo", "Jasper"}
            },
            new object[]
            {
                new[]
                {
                    new Pet(){Name = "Clay", OwnersGender = "Male"},
                    new Pet(){Name = "Snip", OwnersGender = "Female"},
                    new Pet(){Name = "Molly", OwnersGender = "Male"},
                    new Pet(){Name = "Shoppy", OwnersGender = "Male"},
                    new Pet(){Name = "Allen", OwnersGender = "Female"},
                },
                new[] {"***Female***", "Allen", "Snip", "***Male***", "Clay", "Molly", "Shoppy"}
            },
            new object[]
            {
                Enumerable.Empty<Pet>(),
                Enumerable.Empty<string>()
            },
            new object[]
            {
                new[]
                {
                    new Pet() {Name = "Crazy", OwnersGender = "Male"}
                },
                new [] {"***Male***", "Crazy"}
            },
            new object[]
            {
                new[]
                {
                    new Pet() {Name = "Holly", OwnersGender = "Male"},
                            new Pet() {Name = "Ben", OwnersGender = "Female"}
                },
                new [] {"***Female***", "Ben", "***Male***", "Holly"}
            },
            new object[]
            {
                new[]
                {
                    new Pet() {Name = "Holly", OwnersGender = "Female"},
                    new Pet() {Name = "Ben", OwnersGender = "Male"}
                },
                new [] {"***Male***", "Ben", "***Female***", "Holly"}
            }

        };
    }
}
